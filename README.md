# updateTermite

This is a quick script I wrote to update a Termite configuration file with another raw colors file (in my case, the cached output from [pywal](https://github.com/dylanaraps/pywal))

The script assumes you're using the default location of the files, but it's easy to change if you're using a non-standard directory for either your color or configuration files.

## Installation

Written in [python](python.org), with no non-standard libraries. Run and done. Only works on Unix (tested on Ubuntu 17.04).

## Usage

Call it from your .bashrc to make sure termite gets updated each time you color file is updated. If you know it'll be staying the same, you can just call it once.
